
# coding: utf-8

# Bagian 1

# In[43]:

from __future__ import division
import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure, show
import seaborn as sns
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
from numpy import array
from numpy import argmax
from scipy.stats import zscore
import plotly.plotly as py
import plotly.graph_objs as go
import plotly

# In[2]:

plotly.tools.set_credentials_file(username='putra', api_key='uwLIkkj4t1lgQgXjkXd9')
data = pd.read_csv("C:/Users/Babe-PC/kerjaan_hadi/buyingintention/problem1/DATA.csv", sep=";")
data.head()

(total_instances, total_features) = data.shape
print "total instance: " + str(total_instances)
print "total fitur: " + str(total_features)


# In[4]:


#visualisasi distribusi frekuensi hanya untuk variabel dengan tipe kategorikal
# data_plot = data.iloc[:,[0, 1, 2, 3, 11, 12, 13, 15, 16, 17, 19, 22, 26, 27, 29]]
# for col in data_plot:
#     values = data_plot[col].value_counts(sort=False)
#     names = values.keys()
#     plt.bar(range(len(names)), values)
#     plt.xticks(range(len(values)), names)
#     plt.xlabel(col)
#     plt.ylabel("frequency")
#     plt.savefig('df_{}.png'.format(col))
#     plt.show()


# Dengan melihat visualisasi distribusi di atas, didapatkan bahwa:
# 1. Data yang diolah tidak seimbang, yaitu data yang diklasifikasikan sebagai class 0 jauh lebih banyak dibandingkan dengan class 1
# 2. Terdapat beberapa kategori yang imbalanced seperti variabel platform, condition, varUser_1, varUser_2, varUser_3, varSeller_1, varSeller_2, varSeller_3, varSeller_5, varProduct_3, varProduct_4, dan varGeneral_1

# In[6]:


#mengambil data dengan class = 1
data_class_1 = data[data.response == 1]
# data_class_1.head()


# In[7]:


# #eksplorasi data kategorikal
# sns.set_style("whitegrid")
# data_bar_plot = data.iloc[:,[1, 2, 3, 11, 12, 13, 15, 16, 17, 19, 22, 26, 27, 29]]
# for col in data_bar_plot:
#     countplot = sns.countplot(x=col, hue="response", data=data_class_1)
#     plt.savefig('kt_{}.png'.format(col))
#     show()


# In[8]:


# for col in data_bar_plot:
#     count_series = data_class_1.groupby(['response', col]).size()
#     print count_series


# In[9]:


# for col in data_bar_plot:
#     count_series = data_class_1.groupby(['response', col]).size()
#     print max(count_series)


# In[26]:


# mode_categorical_name = data_class_1.category_name.mode()
# print mode_categorical_name
# print data_class_1[data_class_1.category_name == 262].shape


# Dengan melihat grafik dan perhitungan di atas, didapatkan bahwa dari data sampai pengguna memutuskan untuk membeli, yaitu:
# 1. Platform yang paling banyak digunakan adalah apps, yaitu sebanyak 3362
# 2. Kategori yang dilihat oleh user dan paling banyak dibeli adalah 262, yaitu sebanyak 155
# 3. Kondisi barang yang paling banyak dibeli adalah baru, yaitu sebanyak 3813

# In[47]:


#eksplorasi data numerik
data_hist = data.iloc[:,[4]]
for col in data_hist:
    data_class_1[col].hist()
    plt.xlabel(col)
    plt.ylabel("frequency")
    fig = plt.gcf()
    file_name = "histo-{}".format(col)
    plot_url = py.plot_mpl(fig, filename=file_name)